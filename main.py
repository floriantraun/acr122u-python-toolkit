from src import nfc
from src.ExtendedReader import ExtendedReader

if __name__ == '__main__':
	reader = ExtendedReader()

	# Print Mifare Classic strucure
	# reader.mifare_classic_structure()

	# Mute the device
	reader.mute()

	# Authenticate
	reader.authenticate_sector(sector=0, key_value=[0xff for _ in range(6)], key_type=0x60, key_location=0x00)
