from src.ExtendedReader import ExtendedReader


def dump_mifare1k():
	reader = ExtendedReader()
	reader.PRINT_SUCCESS = False

	for i in range(0, 16):
		key = [0xFF for _ in range(6)]  # 6 byte
		reader.authenticate_sector(sector=i, key_value=key)
		reader.dump_sector(sector=i)


if __name__ == '__main__':
	dump_mifare1k()
