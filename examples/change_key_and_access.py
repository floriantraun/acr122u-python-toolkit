from src.ExtendedReader import ExtendedReader

UNDERSTOOD = False

SECTOR = 2  # Change the 3rd sector (sector no 2)
FIRST_BLOCK_OF_SECTOR = SECTOR * ExtendedReader.BLOCKS_PER_SECTOR

DEFAULT_KEY_A = [0xFF for _ in range(6)]  # Mifare cards have 0xFFFFFFFFFFFF as the default key
DEFAULT_KEY_B = [0xFF for _ in range(6)]

NEW_KEY_A = [0xBA, 0xAD, 0xBE, 0xEF, 0xDE, 0xAD]  # new key A is 0xBAADBEEFDEAD
NEW_KEY_B = [0xCA, 0xFE, 0xBA, 0xBE, 0xDE, 0xAD]  # new key B is 0xCAFEBABEDEAD


def change_keys_and_access_conditions():
	"""
	Overwrites the data blocks and changes the keys and access conditions.

	Access conditions are set as follows:
		data block 0   read AB, write B
		data block 1   read AB, write B
		data block 2   read AB, write B
		sector trailer KEYA: read never, write A
		               access bits: read A, write A
					   KEYB: read A, write A

	THIS CODE CAN ONLY BE RUN ONCE SINCE IT STARTS WITH THE DEFAULT KEYS AND OVERWRITES THEM IN THE END.
	RUNNING THIS METHOD A SECOND TIME WILL CAUSE THE AUTHENTICATION TO FAIL SINCE THE KEYS HAVE BEEN CHANGED.
	"""
	# Authenticate with default key
	reader.authenticate_sector(sector=SECTOR, key_value=DEFAULT_KEY_A)

	# Dump the first sector
	reader.dump_sector(SECTOR)

	# Access Bits Calculator: http://calc.gmss.ru/Mifare1k/
	# data block 0   read AB, write B
	# data block 1   read AB, write B
	# data block 2   read AB, write B
	# sector trailer KEYA: read never, write A
	# 				 access bits: read A, write A
	# 				 KEYB: read A, write A
	access_bits = [0xf8, 0x77, 0x80]

	# user data (byte 9)
	user_data = [0x00]  # undefined - can be used for any purpose

	# construct data for sector trailer
	sector_trailer_data = []
	sector_trailer_data.extend(NEW_KEY_A)  # key a (6 bytes)
	sector_trailer_data.extend(access_bits)  # access bits (3 bytes)
	sector_trailer_data.extend(user_data)  # user data (1 byte)
	sector_trailer_data.extend(NEW_KEY_B)  # key b (6 bytes)

	# Overwrite the 3 blocks of data with 0x00 - 0x10 (0 - 15) each
	reader.write(first_block=FIRST_BLOCK_OF_SECTOR, data=[i for i in range(16)])
	reader.write(first_block=FIRST_BLOCK_OF_SECTOR + 1, data=[i for i in range(16)])
	reader.write(first_block=FIRST_BLOCK_OF_SECTOR + 2, data=[i for i in range(16)])

	# write sector trailer
	reader.write(first_block=FIRST_BLOCK_OF_SECTOR + 3, data=sector_trailer_data)


if __name__ == '__main__':
	if UNDERSTOOD is True:
		reader = ExtendedReader()

		# Don't print success messages
		reader.PRINT_SUCCESS = True

		# Mute device
		reader.mute()

		# Change data, keys and access conditions
		change_keys_and_access_conditions()

		# Authenticate with key A for reading datablocks
		reader.load_authentication_data(key_location=0x00, key_value=NEW_KEY_A)
		reader.authentication(block_number=FIRST_BLOCK_OF_SECTOR, key_location=0x00, key_type=0x60)

		reader.dump_sector(SECTOR)
	else:
		print("ATTENTION")
		print("Be aware that this code changes the keys and access bits to some sectors of the card as well as overwrites some data.")
		print("More about that can be analyized in the `change_keys_and_access_conditions` method, it is well commented.")
		print("To run the code, set the `UNDERSTOOD` variable at the beginning of the file to `True`")