from src import nfc, error


class ExtendedReader(nfc.Reader):
	BLOCK_SIZE: int = 16
	BLOCKS_PER_SECTOR: int = 4
	SECTORS: int = 16

	PRINT_SUCCESS = True

	def __init__(self, *args, **kwargs):
		super(ExtendedReader, self).__init__(*args, **kwargs)

	def authenticate_sector(self, sector: int = 0, key_value: list = None, key_type: int = 0x60,
							key_location: int = 0x00) -> bool:
		"""
		Authenticate a sector
		"""
		first_block_of_sector = sector * self.BLOCKS_PER_SECTOR

		self.load_authentication_data(key_location=key_location, key_value=key_value)
		self.authentication(block_number=first_block_of_sector, key_location=key_location, key_type=key_type)

		return True

	def read(self, first_block: int = 0x01, number_of_bytes_to_read: int = BLOCK_SIZE) -> list:
		"""
		Read from the card
		"""
		# Check if number_of_bytes_to_read is multiple of BLOCK_SIZE
		if (number_of_bytes_to_read % self.BLOCK_SIZE) != 0:
			raise error.InstructionFailed(f"Only multiples of {self.BLOCK_SIZE} (block size) can be read.")

		# Enumerate through bytes
		rx = []
		current_block = first_block

		while number_of_bytes_to_read >= self.BLOCK_SIZE:
			rx.append(self.read_binary_blocks(block_number=current_block, number_of_byte_to_read=self.BLOCK_SIZE))

			# Decrement bytes to read by BLOCK_SIZE
			number_of_bytes_to_read -= self.BLOCK_SIZE

			# Increment current block to read next block
			current_block += 1

		# Return
		return rx

	def write(self, first_block: int = 0x01, data: list = None) -> bool:
		"""
		Write to the card
		"""
		# Set number_of_bytes_to_write to the length of the data
		number_of_bytes_to_write = len(data)

		# Set number_of_bytes_to_write to BLOCK_SIZE if smaller than BLOCK_SIZE
		if number_of_bytes_to_write < self.BLOCK_SIZE:
			number_of_bytes_to_write = self.BLOCK_SIZE

		# Set to multiple of BLOCK_SIZE if not already
		if number_of_bytes_to_write % self.BLOCK_SIZE != 0:
			number_of_bytes_to_write = self.BLOCK_SIZE * ((number_of_bytes_to_write // self.BLOCK_SIZE) + 1)

		# Enumerate through bytes
		current_block = first_block
		pos = 0
		while number_of_bytes_to_write >= self.BLOCK_SIZE:
			# Only write bytes which are not yet written
			data_to_write = data[pos:]

			# Write
			self.update_binary_blocks(block_number=current_block, number_of_byte_to_update=self.BLOCK_SIZE, block_data=data_to_write)

			# Decrement bytes to read by BLOCK_SIZE
			number_of_bytes_to_write -= self.BLOCK_SIZE

			# Increment current block to read next block
			current_block += 1

			# Increment position by the already written bytes
			pos += self.BLOCK_SIZE

		return True

	def mute(self) -> bool:
		"""
		Disables the readers beep
		"""
		self.buzzer_sound(0)
		return True

	def unmute(self) -> bool:
		"""
		Enables the readers beep
		"""
		self.buzzer_sound(1)
		return True

	def dump_sector(self, sector: int = 0x00) -> bool:
		"""
		Dump a whole sector
		"""
		first_block = sector
		number_of_bytes_to_read = self.BLOCK_SIZE * self.BLOCKS_PER_SECTOR

		# Increase the first block if the sector is not 0x00
		if sector > 0x00:
			first_block = (sector * 4)

		# Read
		rx = self.read(first_block=first_block, number_of_bytes_to_read=number_of_bytes_to_read)

		# Print the dump
		print(f"Sector {sector}")
		for block, _ in enumerate(rx):
			current_block = block + first_block
			print(f"Block {hex(current_block)} [{current_block}]\t{rx[block]}")

		return True

	@staticmethod
	def text_to_data(string: str) -> list:
		"""
		Convert an ASCII string to a list with the corresponding values
		"""
		return [ord(char) for char in string]

	@staticmethod
	def mifare_classic_structure() -> bool:
		"""
		Print the structure of MIFARE Classic
		"""
		# https://docs.onion.io/omega2-docs/using-rfid-nfc-expansion.html#using-mifare-classic-cards
		# https://shop.sonmicro.com/Downloads/MIFARECLASSIC-UM.pdf

		print("==== Mifare Classic ====")
		print("[ 1K ] 16 Sectors	  4 Blocks per sector	   16 bytes per block  1024 bytes total")
		print("[ 4K ] 32[+8] Sectors  4[/16] Blocks per sector  16 bytes per block  4096 bytes total\n")

		print("Sector 0 (1K / 4K)")
		print("	Block 0x00 [0]: aaaa aaaa bbcc ccdd dddd dddd dddd dddd | read-only")
		print("	Block 0x01 [1]: 0000 0000 0000 0000 0000 0000 0000 0000")
		print("	Block 0x02 [2]: 0000 0000 0000 0000 0000 0000 0000 0000")
		print("	Block 0x03 [3]: eeee eeee eeee ffff ffgg hhhh hhhh hhhh")
		print("		a... 4 Bytes, UID")
		print("		b... 1 Byte,  BCC")
		print("		c... 2 Bytes, ATQA")
		print("		d... 9 Bytes, Manufacturer Data")
		print("		0...32 Bytes, Stored Data")
		print("		e... 6 Bytes, Key A (mandatory)")
		print("		f... 3 Bytes, Access Conditions")
		print("		g... 1 Byte,  General Purpose (Stored data)")
		print("		h... 6 Bytes, Key B (optional)")

		print("Sectors 1-15 (1K), Sectors 1-31 (4K)")
		print("	Block 0x04 [4]: 0000 0000 0000 0000 0000 0000 0000 0000")
		print("	Block 0x05 [5]: 0000 0000 0000 0000 0000 0000 0000 0000")
		print("	Block 0x06 [6]: 0000 0000 0000 0000 0000 0000 0000 0000")
		print("	Block 0x07 [7]: eeee eeee eeee ffff ffgg hhhh hhhh hhhh")
		print("		0...48 Bytes, Stored Data")
		print("		e... 6 Bytes, Key A (mandatory)")
		print("		f... 3 Bytes, Access Conditions")
		print("		g... 1 Byte,  General Purpose (Stored data)")
		print("		h... 6 Bytes, Key B (optional)")

		print("Sectors 32-39 (4K)")
		print("	Block 0x80 [128]: 0000 0000 0000 0000 0000 0000 0000 0000")
		print("	Block 0x81 [129]: 0000 0000 0000 0000 0000 0000 0000 0000")
		print("	Block 0x82 [130]: 0000 0000 0000 0000 0000 0000 0000 0000")
		print("	Block 0x.. [...]: 0000 0000 0000 0000 0000 0000 0000 0000")
		print("	Block 0x.. [...]: 0000 0000 0000 0000 0000 0000 0000 0000")
		print("	Block 0x8F [143]: eeee eeee eeee ffff ffgg hhhh hhhh hhhh")
		print("		0...240 Bytes, Stored Data")
		print("		e... 6 Bytes, Key A (mandatory)")
		print("		f... 3 Bytes, Access Conditions")
		print("		g... 1 Byte,  General Purpose (Stored data)")
		print("		h... 6 Bytes, Key B (optional)")

		return True
