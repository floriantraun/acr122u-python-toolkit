# ACR122U Python Toolkit

<img src="http://downloads.acs.com.hk/product-website-image/acr38-image.jpg" width="150" height="150">

Python library for working with the ACS ACR122U smart card reader/writer.

## Installation
```bash
git clone https://gitlab.com/floriantraun/acr122u-python-toolkit/
cd acr122u-python-toolkit
pip install -r requirements.txt
```

## Usage

The `ExtendedReader` class provides higher level methods for interacting with the device.

The `ExtendedReader` class is a child class of `nfc.Reader` and supports the same methods as well as additional methods.

```python
from src.ExtendedReader import ExtendedReader

# Instantiate a new ExtendedReader object
reader = ExtendedReader()

# Mute reader buzzer
reader.mute()

# Get UID and print in dec, hex and ASCII
reader.print_data(reader.get_uid())

# Receive the first block
rx = reader.read(first_block=0x01, number_of_bytes_to_read=16)

# Print the first block
print(rx)
```

### List of methods

High-Level methods

- `authenticate_sector` - Authenticates a sector
- `read` - Reads from the card - checks for valid arguments
- `write` - Writes to the card - checks for valid arguments
- `mute` - Disables the readers beep
- `unmute` - Enables the readers beep
- `dump_sector` - Prints the contents of a sector (this method does not authenticate so this might be required to do beforehand)
- `@static text_to_data` - Convert an ASCII string to a list with the corresponding values
- `@static mifare_classi_structure` - Print the structure of MIFARE Classic

Low-level methods provided by the underlying `nfc` library.

- `@static instantiate_reader` - Called by constructor and instantiates the reader and connection
- `command` - Send a payload of a predefined command to the reader
- `custom` - Send a custom payload to the reader
- `get_uid` - Get the UID of the card
- `firmware_version` - Get the firmware version of the reader
- `load_authentication_data` - Load the authentication key
- `authentication` - Authentication with the key in `load_authentication_data`
- `read_binary_blocks` - Reads `n` bytes from the card at the `block_number` index
- `update_binary_blocks` - Update `n` bytes in the card with `block_data` at the `block_number` index
- `led_control` - Control LED state
- `get_picc_version` - Get the PICC version of the reader
- `set_picc_version` - Set the PICC version of the reader
- `buzzer_sound` - Set the buzzer sound state
- `set_timeout` - Set the timeout of the reader
- `info` - Print the type of the card on the reader
- `@static print_data` - Print the data in decimal, hexadecimal and string form
- `@static print_sw1_sw2` - Print sw1 and sw2

## Kudos

- [Flowtter/py-acr122u](https://github.com/Flowtter/py-acr122u) for `nfc.Reader`
